#!/bin/bash
state=`ip link | grep can0 | awk '{for(i=1;i<=NF;i++)if($i=="state")print $(i+1)}'`
if [ "${state}" = "UP" ]; then
	sudo ip link set can0 down
fi
sudo ip link set can0 up type can bitrate 667000
sudo ifconfig can0 txqueuelen 1000