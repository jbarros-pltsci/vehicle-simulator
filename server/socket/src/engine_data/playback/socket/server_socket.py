import socket
import struct
import logging
import json
import threading
import signal
import time
import sys
import os
from pathlib import Path

HOST = ''  # Standard loopback interface address (localhost)
PORT = 8080 # Port to listen on (non-privileged ports are > 1023)

isThreadAliveEvent = threading.Event()

logging.basicConfig(format='%(asctime)s : %(levelname)s : %(message)s', level=logging.INFO)

def json_serialize(obj):
    """
    JSON serializer for objects not serializable by default json code, adds support for converting
    datetime objects to string using the ISO format

    Args:
        obj (object): the object to serialize

    Returns:
        str: ISO format datetime
    """
    if isinstance(obj, datetime):
        serial = obj.isoformat()
        return serial
    raise TypeError ("Type not serializable")

class _Tx(threading.Thread):

    def __init__(self, conn, isThreadAliveEvent):
        threading.Thread.__init__(self)
        self.conn = conn
        self.isThreadAliveEvent = isThreadAliveEvent
    
    @staticmethod
    def get_current_time():
        return int(time.time() * 1000)

    @staticmethod    
    def _update_timestamp(data):
        """
        TODO: FIX: location.timestamp shouldn't get updated
        """
        current_time = _Tx.get_current_time()

        for (k, v) in data.items():
            if (k == 'timestamp') or (k == 'updated_at'):
                data[k] = current_time
            if type(v) is dict:
                _Tx._update_timestamp(data[k])
            if type(v) is list:
                for nestedObj in data[k]:
                    _Tx._update_timestamp(nestedObj)

    @staticmethod
    def _load_engine_events():
        currentDir = os.path.dirname(os.path.realpath(__file__))
        filename = Path(currentDir).parent.parent / 'engine_events.json'
        
        logging.debug(f'Filename {filename}')

        with open(filename) as json_file:
            dataArray = json.load(json_file)
            for data in dataArray:
                logging.info(data)
                _Tx._update_timestamp(data)
            return dataArray
        
    @staticmethod
    def _send(conn):
        dataArray = _Tx._load_engine_events()
        for dataObject in dataArray:
            data = json.dumps(dataObject, indent=2, sort_keys=True, default=json_serialize).encode()
            msgSize = len(data)
            packed = struct.pack('!I%ss' % msgSize, msgSize, data)
        
            logging.debug(f'Sending json {data}')

            conn.sendall(packed)

            time.sleep(1)

    def run(self):
        while not self.isThreadAliveEvent.wait(5):
            _Tx._send(self.conn)            
        
        logging.info('Tx thread finished')
        raise SystemExit

class _Rx(threading.Thread):

    def __init__(self, conn, isThreadAliveEvent):
        threading.Thread.__init__(self)
        self.conn = conn
        self.isThreadAliveEvent = isThreadAliveEvent
    
    @staticmethod
    def _read(conn):
        header = conn.recv(4)
        if len(header) == 0:        
            raise RuntimeError('Header is empty')
        
        payloadSize = struct.unpack('!I', header)[0]
        if payloadSize <= 0 or payloadSize > 200000:        
            raise RuntimeError('Payload size out of boundary (1-200000)! Payload received size: %s', payloadSize)
        
        chunks = []
        bytesRead = 0

        while bytesRead < payloadSize:
            chunk = conn.recv(payloadSize - bytesRead)
            if chunk == b'':
                raise RuntimeError("Socket connection broken")
            
            chunks.append(chunk)
            bytesRead += len(chunk)

        return json.loads(b''.join(chunks))
        
    def run(self):
        try:
            while not self.isThreadAliveEvent.is_set():
                messageReceived = _Rx._read(self.conn)
                logging.debug(messageReceived)
        except Exception as e:
            logging.error(e)
        
        logging.info('Rx thread finished')
        raise SystemExit

def _exit(signum, frame):
    isThreadAliveEvent.set()
    sys.exit(signum)

def main():
    logging.info('Starting Server Socket')
    
    signal.signal(signal.SIGINT, _exit)

    with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as socket_conn:
        socket_conn.bind((HOST, PORT))
        while True:
            socket_conn.listen()        
            conn, addr = socket_conn.accept()
            with conn:                
                logging.info(f'Connected by {addr}')

                isThreadAliveEvent.clear()

                rxThread = _Rx(conn, isThreadAliveEvent)
                rxThread.start()
                
                txThread = _Tx(conn, isThreadAliveEvent)
                txThread.start()

                while rxThread.is_alive() and txThread.is_alive():
                    time.sleep(1)
                
                logging.info('Stoping rxThread and/or txThread')

                isThreadAliveEvent.set()

if __name__ == "__main__":
    main()