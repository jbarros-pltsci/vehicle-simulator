import socket
import struct
import logging
import json
import threading
import signal
import time
import sys
import os
from pathlib import Path

logging.basicConfig(format='%(asctime)s : %(levelname)s : %(message)s', level=logging.DEBUG)

def json_serialize(obj):
    """
    JSON serializer for objects not serializable by default json code, adds support for converting
    datetime objects to string using the ISO format

    Args:
        obj (object): the object to serialize

    Returns:
        str: ISO format datetime
    """
    if isinstance(obj, datetime):
        serial = obj.isoformat()
        return serial
    raise TypeError ("Type not serializable")

class _Tx(threading.Thread):

    def __init__(self):
        threading.Thread.__init__(self)
        # self.conn = conn
        # self.isThreadAliveEvent = isThreadAliveEvent
    
    @staticmethod
    def get_current_time():
        return int(time.time() * 1000)

    @staticmethod    
    def _update_timestamp(data):
        """
        TODO: FIX: location.timestamp shouldn't get updated
        """
        current_time = _Tx.get_current_time()

        for (k, v) in data.items():
            if (k == 'timestamp') or (k == 'updated_at'):
                data[k] = current_time
            if type(v) is dict:
                _Tx._update_timestamp(data[k])
            if type(v) is list:
                for nestedObj in data[k]:
                    _Tx._update_timestamp(nestedObj)

    @staticmethod
    def _load_engine_events():
        currentDir = os.path.dirname(os.path.realpath(__file__))
        filename = Path(currentDir).parent.parent / 'engine_events.json'
        
        logging.debug(f'Filename {filename}')

        with open(filename) as json_file:
            dataArray = json.load(json_file)
            for data in dataArray:
                logging.info(data)
                _Tx._update_timestamp(data)
            return dataArray
        
    @staticmethod
    def _send():
        dataArray = _Tx._load_engine_events()
        for dataObject in dataArray:
            data = json.dumps(dataObject, indent=2, sort_keys=True, default=json_serialize).encode()
            msgSize = len(data)
            packed = struct.pack('!I%ss' % msgSize, msgSize, data)
        
            logging.debug(f'Sending json {data}')

            # conn.sendall(packed)

            time.sleep(1)

    def run(self):
        # while not self.isThreadAliveEvent.wait(5):
            # _Tx._send(self.conn)            
        _Tx._send()
        logging.info('Tx thread finished')
        raise SystemExit

def main():
    txThread = _Tx()
    txThread.start()
    # dataObject = _load_engine_events()
    # data = json.dumps(dataObject)
    # print(data)
    # msgSize = len(data)

    # packed = struct.pack('!I%ss' % msgSize, msgSize, data.encode())
    # print(packed)

if __name__ == "__main__":
    main()